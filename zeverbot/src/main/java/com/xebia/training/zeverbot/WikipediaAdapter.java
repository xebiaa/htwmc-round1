package com.xebia.training.zeverbot;

import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * @author iwein
 */
public class WikipediaAdapter {
    private final RestTemplate restTemplate;

    private final SentenceExtractor sentenceExtractor = new SentenceExtractor();

    public WikipediaAdapter(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<String> getSentencesFor(String query) {
        String result = restTemplate.
                getForObject("http://en.wikipedia.org/w/index.php?search={query}",
                        String.class,
                        query);

        return sentenceExtractor.extractSentences(result);
    }
}
