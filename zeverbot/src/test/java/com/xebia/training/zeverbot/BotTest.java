package com.xebia.training.zeverbot;
import com.xebia.training.zeverbot.Bot;
import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * @author iwein
 */
public class BotTest {

    private final Bot bot = new Bot();

    @Test
    @Ignore //remove this line and make the build green again
    public void shouldRespondToGreeting() throws Exception {
        assertThat(bot.respondTo("Hello"), is("Hello"));
        //add more tests or modify this test for more elaborate greeting procedures
    }

}
