Instructions for the labs
=========================


Follow these instructions to get started with the labs, whenever you run into trouble with git or the tooling ask the instructor.

Set up your environment
-----------------------
Your environment should be preinstalled with Java, Maven, Eclipse and a Git client ([MSysgit](http://code.google.com/p/msysgit/)). You can verify this by running Git Bash and running `java -version` from the bash shell. You can run Eclipse too to verify that it is installed correctly.

Now take the following steps to get up and running:

1. Get a git account and follow [the setup guide](http://help.github.com/win-set-up-git/) from the step "Set up SSH keys"
   (this assumes git and tooling are pre-installed)
2. Ask the instructor to give you push access to the github repository
3. Clone the project `git clone git@github.com:xebia/htwmc-round1.git`
4. Start your IDE and import the project
5. Create a local branch for development and check it out `git checkout -b <my-branch>`
6. Start working

Whenever you're ready to commit something you can use `git add` to add what you want to commit to the index and `git commit` to commit locally. We'll discuss pushing in a later lab. If you get in trouble, ask the instructor or refer to a git cheat sheet (there are plenty online, the [the overview image](http://osteele.com/images/2008/git-transport.png) that was used in the opening presentation is nice and concise). You can run `git help <command>` to open the standard documentation for any command.

Below follow detailed instructions for today

Requirements
============

- [MSysGit](http://code.google.com/p/msysgit/)
- [JDK 1.6](http://java.sun.com/)
- [Maven 3](http://maven.apache.org)
- A Java IDE (typically [Eclipse](http://eclipse.org/))

Installation
============

De software installers staan op `i:\training - how to write maintainable code`.

Werk bij voorkeur op een ONT3 werkplek

MSysGit
-------
* Installeren op `c:\training\software\git`
* Installatiecomponenten:
    * Context menu entries
* Use Git Bash only
* AutoCRLF: kies voor "as is, as is"
* Doorklikken tot en met Finish
 
Proxyserver instellingen
------------------------
* Kopieer het `profile` bestand van de i-schijf naar `U:\.profile`)
    * Wat dit doet is het aanmaken van een `https_proxy` environment variabele de volgende keer dat je Git Bash start
* Voeg toe onderaan het bestand: `export MAVEN_HOME=/c/work/tooling/buildtool/apache-maven-3.0.3`

Java en Eclipse
---------------
* Zijn reeds op het virtueeltje aanwezig en kunnen worden hergebruikt. (ergens onder `c:\work`)

Maven 3
-------
* Staat in `c:\work\tooling\buildtool\apache-maven-3.0.3`
    * Aanpassen `conf/settings.xml`, toevoegen proxyconfiguratie vanuit de settings.xml file op `i:\training ...`
    * Toevoegen `<profile><id>no-nexus</id></profile>`
    * Onderaan bij `<activeProfile>` het nieuwe `no-nexus` profile actief maken.
* Controleren dat `$MAVEN_HOME/bin` in je PATH variabele staat

Git configureren
----------------
* Vanuit git bash:
    * `git config --global user.name "Pietje Puk"`
    * `git config --global user.email "PietjePuk@example.com"`

Project uitchecken
------------------
* Vanuit git bash:
    * Controleer dat `$https_proxy` bestaat en de hostname en port van de proxyserver bevat.
    * `git clone https://<username>@github.com/xebia/htwmc-round1.git`
    * `mvn install`
    * `mvn eclipse:eclipse -DdownloadSources=true`
* Eclipse opstarten
* Import project
    * Eventueel via de workspace properties de `M2_REPO` variabele aanmaken.

